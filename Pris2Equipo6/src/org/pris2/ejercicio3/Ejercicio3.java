package org.pris2.ejercicio3;

public class Ejercicio3 {
	int xizq;
    int xder;
    double precision;
    double xr;
    double funcionDer;
    double funcionIzq;
    double funcionRaiz;   
    
    public Double funcionDerecha(int xderecha){
    	funcionDer=Math.pow(xderecha, 5)-Math.pow(xderecha, 4)+Math.pow(xderecha, 3)-3;     
        return funcionDer;
    }
    
    public Double funcionIzquierda(int xizquierda){
    	funcionIzq=Math.pow(xizquierda, 5)-Math.pow(xizquierda, 4)+Math.pow(xizquierda, 3)-3;      
        return funcionIzq;
    }
    
    public Double xr(int xizq,int xder,double funcionIzq,Double funcionDer){
        xr=xder-funcionDer*(xizq-xder)/(funcionIzq-funcionDer);  
        return xr;
    }
    
   public Double funcionRaiz(double xr){
	   funcionRaiz=Math.pow(xr, 5)-Math.pow(xr, 4)+Math.pow(xr, 3)-3;  
       return funcionRaiz;
   }
}