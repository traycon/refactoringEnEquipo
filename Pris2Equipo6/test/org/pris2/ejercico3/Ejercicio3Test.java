package org.pris2.ejercico3;

import org.pris2.ejercicio3.Ejercicio3;

import junit.framework.TestCase;

public class Ejercicio3Test extends TestCase {

	int xizq = 5;
	int xder = 84;
	double precision;
	double xr = 4.999949880984545;
	double funcionDer = 4.132924989E9;;
	double funcionIzq = 2622.0;

	public void testEjercicio3XIzq() {
		Ejercicio3 ejer = new Ejercicio3();
		ejer.funcionIzquierda(xizq);
		assertEquals(2622.0, ejer.funcionIzquierda(xizq));
	}

	public void testEjercicio3XDer() {
		Ejercicio3 ejer = new Ejercicio3();
		ejer.funcionDerecha(xder);
		assertEquals(4.132924989E9, ejer.funcionDerecha(xder));
	}

	public void testXr() {
		Ejercicio3 ejer = new Ejercicio3();
		ejer.xr(xizq, xder, funcionIzq, funcionDer);
		assertEquals(4.999949880984545, ejer.xr(xizq, xder, funcionIzq, funcionDer));
	}

	public void testFuncionRaiz() {
		Ejercicio3 ejer = new Ejercicio3();
		assertEquals(2621.864681459028, ejer.funcionRaiz(xr));
	}
}